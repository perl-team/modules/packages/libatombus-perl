Source: libatombus-perl
Section: perl
Priority: optional
Build-Depends: cdbs,
 licensecheck,
 perl,
 debhelper (>= 10~),
 dh-buildinfo,
 libatompub-perl,
 libdancer-perl,
 libdancer-plugin-dbic-perl,
 libdbd-sqlite3-perl,
 libdbix-class-perl,
 libsql-translator-perl,
 libuuid-tiny-perl,
 libxml-atom-perl,
 libxml-xpath-perl,
 libcapture-tiny-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.8
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libatombus-perl.git
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libatombus-perl
Homepage: https://metacpan.org/release/AtomBus
Testsuite: autopkgtest-pkg-perl

Package: libatombus-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}, ${cdbs:Depends}
Description: AtomPub server for messaging
 The Atom Publishing Protocol (Atompub) is a protocol for publishing and
 editing Web resources described at http://www.ietf.org/rfc/rfc5023.txt.
 .
 AtomBus is an AtomPub server that can be used for messaging. The idea
 is that atom feeds can correspond to conceptual queues or buses.
 AtomBus is built on top of the Dancer framework. It is also
 PubSubHubbub (PuSH) friendly.
